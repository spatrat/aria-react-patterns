import React from "react";

const Option = props => {
  return (
    <li
      className={`art-select__option${
        props.selected ? " art-select__option--selected" : ""
      }`}
      role="menuitem"
      id={props.rsl_id}
      onClick={props.onClick}
      tabIndex={-1}
    >
      {props.name}
    </li>
  );
};

export default Option;

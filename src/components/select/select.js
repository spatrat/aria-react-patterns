import React from "react";
import Option from "./option";

// TODO : Add close capability on click outside and Escape key pressed
// TODO : Remove hardcoded id from select button and find a way to add a dynamic id to it
// TODO: Remove test from state
// TODO : Implement with Flux state management ?

class Select extends React.Component {
  constructor(props) {
    super(props);
    this.select = null;
    this.button = null;

    this.setSelectRef = element => {
      this.select = element;
    };

    this.buttonRef = element => {
      this.button = element;
    };

    props.options.map(option => {
      const id =
        "rsl_option_" +
        new Date().getMilliseconds() * Math.floor(Math.random() * 100 + 10) +
        option.name.replace(/ /g, "_");
      return (option.rsl_id = id);
    });

    this.state = {
      options: props.options,
      selectedOptionId: null,
      selectedOption: null,
      listIsOpen: false,
      test: "TOUJOURS ICI ! - STRING POUR TESTER LES MERGE DE STATE"
    };

    this.selectedOptionIndex = 0;
  }

  componentDidMount() {
    let selectedOption = this.getSelectedOption(this.state.options);
    let selectedOptionId =
      selectedOption.rsl_id || this.props.options[0].rsl_id;

    this.setState(
      {
        selectedOptionId,
        selectedOption
      },
      () => {
        console.log("STATE IS UPDATED ON MOUNT : ", this.state);
      }
    );
  }

  closeDropdown = (cb = null) => {
    this.setState(
      {
        listIsOpen: false
      },
      () => {
        cb && cb();
      }
    );
  };

  focusOnSelect = () => {
    this.setState(
      {
        listIsOpen: !this.state.listIsOpen
      },
      () => {
        if (this.select) this.select.focus();
        this.selectedOptionIndex = -1;
      }
    );
  };

  keyDownOnOption = event => {
    event.preventDefault();
    const options = [...this.select.childNodes];
    const itemToFocus = options[this.selectedOptionIndex];

    if (event.keyCode === 13) {
      this.selectItem(itemToFocus);
      this.validateSelection();
    } else if (event.keyCode === 40) {
      if (this.selectedOptionIndex < options.length - 1) {
        this.selectedOptionIndex += 1;
        this.setFocus(options[this.selectedOptionIndex]);
        options[this.selectedOptionIndex].focus();
      } else {
        this.selectedOptionIndex = 0;
        this.setFocus(options[this.selectedOptionIndex]);
        options[this.selectedOptionIndex].focus();
      }
      //console.log(this.selectedOptionIndex);
    } else if (event.keyCode === 38) {
      //console.log(prevOption);
      if (this.selectedOptionIndex > 0) {
        this.selectedOptionIndex -= 1;
        this.setFocus(options[this.selectedOptionIndex]);
        options[this.selectedOptionIndex].focus();
      } else {
        this.selectedOptionIndex = options.length - 1;
        this.setFocus(options[this.selectedOptionIndex]);
        options[this.selectedOptionIndex].focus();
      }
    } else if (event.keyCode === 27) {
      this.closeDropdown(() => {
        this.button.focus();
      });
    }
  };

  setFocus = item => {
    if (this.select.scrollHeight > this.select.clientHeight) {
      const scrollBottom = this.select.clientHeight + this.select.scrollTop;
      const elementBottom = item.offsetTop + item.offsetHeight;
      if (elementBottom > scrollBottom) {
        this.select.scrollTop = elementBottom - this.select.clientHeight;
      } else if (item.offsetTop < this.select.scrollTop) {
        this.select.scrollTop = item.offsetTop;
      }
    }

    [...this.select.childNodes].forEach(node => {
      node.classList.remove("art-select__option--focused");
    });
    item.classList.add("art-select__option--focused");
  };

  selectItem = item => {
    const selectedOptionId = item.getAttribute("id");
    this.setSelectedOption(selectedOptionId);
  };

  validateSelection = () => {
    this.closeDropdown(() => {
      const selectedOption = this.state.options.filter(
        option => option.selected === true
      )[0];
      this.button.focus();
      this.props.onSelection(() => {
        console.log("OPTION HAS BEEN SELECTED : ", selectedOption);
      });
    });
  };

  setSelectedOption = id => {
    const options = [...this.state.options];
    const newOptions = options.reduce(
      (newsState, option) => {
        if (option.rsl_id === id) {
          option.selected = true;
          newsState.selectedOption = option;
          newsState.selectedOptionId = option.rsl_id;
        } else {
          option.selected = false;
        }
        newsState.options.push(option);
        return newsState;
      },
      {
        options: [],
        selectedOption: null,
        selectedOptionId: null
        //listIsOpen: false
      }
    );

    this.setState(Object.assign({}, this.state, newOptions), () => {
      //this.button.focus();
    });
  };

  getSelectedOptionById = id => {
    return this.state.options.filter(option => {
      return option.rsl_id === id;
    })[0];
  };

  getSelectedOption = () => {
    return this.state.options.filter(option => {
      if (option.selected) {
        console.log(option);
        return option.selected === true;
      }
      return this.state.options[0];
    })[0];
  };

  handleClickOnOption = event => {
    event.preventDefault();
    const id = event.target.id;
    this.setSelectedOption(id);
    this.validateSelection();
  };

  setOptions = options => {
    return options.map((option, i) => {
      return (
        <Option
          key={option.rsl_id}
          onClick={this.handleClickOnOption}
          {...option}
        />
      );
    });
  };

  render() {
    const optionsElements = this.setOptions(this.props.options);
    const selectedOption = this.state.selectedOption
      ? this.state.selectedOption.name
      : "Loading...";
    return (
      <div className="rs-listbox">
        <span id="rsl__desc">Choose an element:</span>
        <div id="rsl__wrapper">
          <button
            aria-haspopup="true"
            aria-label={
              "Choississez un élément dans la liste. Élément selectionné: " +
              selectedOption
            }
            aria-expanded={this.state.listIsOpen}
            className="rsl__button"
            id="rsl__button"
            ref={this.buttonRef}
            onClick={this.focusOnSelect}
          >
            {/* <span className="sr-only">Élément selectionné: </span> */}
            {selectedOption}
            &nbsp;{" "}
            <span className="rsl__button__icon" aria-hidden>
              &#9662;
            </span>
          </button>
          <ul
            className={`rsl__list ${
              this.state.listIsOpen ? "rsl__list--open" : ""
            }`}
            role="menu"
            tabIndex={0}
            ref={this.setSelectRef}
            onKeyDown={this.keyDownOnOption}
            aria-activedescendant={this.state.selectedOptionId}
          >
            {optionsElements}
          </ul>
        </div>
      </div>
    );
  }
}

export default Select;

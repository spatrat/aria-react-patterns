import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

// Componenets
import Select from "./components/select";

const options = [
  {
    name: "test option 1",
    value: "test option 1 value",
    enabled: true,
    selected: true
  },
  {
    name: "test option 2",
    value: "test option 2 value",
    enabled: true,
    selected: false
  },
  {
    name: "test option 3",
    value: "test option 3 value",
    enabled: true,
    selected: false
  },
  {
    name: "test option 4",
    value: "test option 4 value",
    enabled: true,
    selected: false
  },
  {
    name: "test option 5",
    value: "test option 5 value",
    enabled: true,
    selected: false
  },
  {
    name: "test option 6",
    value: "test option 6 value",
    enabled: true,
    selected: false
  }
];

class App extends Component {
  handleSelectSelection = cb => {
    cb && cb();
    console.log("Handle select selection");
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Select options={options} onSelection={this.handleSelectSelection} />
      </div>
    );
  }
}

export default App;
